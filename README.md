# Clojurescript SPA project template

For more information about this project template, read this [awesome post by Christian Johansen](https://cjohansen.no/tools-deps-figwheel-main-devcards-emacs/)

Please refer to the usage section to generate a project, then cd to it, and look at README.md for further instructions

## Usage

### Direct

`clj -m clj-new.create https://gitlab.com/pbaille/spa@{{last commit sha}} {{your project name (qualified symbol)}}`

### Using user alias

add this to your `~/.clojure/deps.edn` file

nb: make sure you replace {{last commit sha}} with appropriate value

``` clojure
{...

 :aliases 
 {...
  ;; --- COPY START ---
  :new-spa
  {:extra-deps {seancorfield/clj-new {:mvn/version "0.5.5"}}
   :main-opts ["-m" "clj-new.create" "https://gitlab.com/pbaille/spa@{{last commit sha}}"]}}
  ;; --- COPY END -----
 }
}
```

now if you can:

`clj -A:new-spa me/awesome-proj`

## Roadmap

- [ ] add a deploy to clojars script
- [ ] parametrize react wrapper (reagent, re-frame or rum)
- [ ] full stack version

## License

why?



# {{name}}

For more information about this generated project structure, read this [awesome post by Christian Johansen](https://cjohansen.no/tools-deps-figwheel-main-devcards-emacs/)

## Dev

launching a browser connected repl from your editor or the terminal 
this will build your project and open your app in a new browser tab pointing to
`http://localhost:9500`

You can code against your app or use the dedicated devcards url 
`http://localhost:9500/figwheel-extra-main/devcards`

### Emacs

navigate to a cljs namespace the `cider-jack-in-cljs`

### Cursive

open this project with cursive

a window should pop, check `Use auto-import`

wait a bit for intellij to sync

go to `Run` then `edit configurations`

check `clojure.main` and `Run with Deps`
feel the `Aliases` field with `dev:repl`

apply changes and close

launch the repl, enjoy! :)

### Others

launching a dev repl from the root of the project
`clj -A:dev:repl`

## Test

run tests using kaocha-cljs
`make test`

you should see a nice test report on 
`http://localhost:9500/figwheel-extra-main/tests`


## Deploy

deploy to clojars
`make deploy`

## pom and jar

generate pom.xml
`make pom.xml`

generate jar
`make {{class}}.jar`


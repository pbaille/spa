(ns {{class}}.styles 
    (:require [stylefy.core :as stylefy]))

(def header
  {:padding "25px"
   :font-family "sans-serif"
   :color "tomato"
   :background "#FAFAFA"})

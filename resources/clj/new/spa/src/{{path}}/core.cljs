(ns {{class}}.core
    (:require
     [reagent.core :as r]
     [{{class}}.styles :as ss]
     [stylefy.core :as stylefy
      :refer [use-style sub-style use-sub-style]]))

(def state (r/atom {:greeting "Hello world!"}))

(defn add [a b]
  (+ a b))

(defn root-comp []
  [:h1
   (use-style ss/header)
   (:greeting @state)])

(defn ^:export init []
  (stylefy/init)
  (r/render [root-comp]
            (js/document.getElementById "app")))


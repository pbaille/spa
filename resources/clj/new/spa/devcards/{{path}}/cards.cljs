(ns ^:figwheel-hooks {{class}}.cards
  (:require [devcards.core]
            [{{class}}.cards.first]))

(enable-console-print!)

(defn render []
  (devcards.core/start-devcard-ui!))

(defn ^:after-load render-on-relaod []
  (render))

(render)

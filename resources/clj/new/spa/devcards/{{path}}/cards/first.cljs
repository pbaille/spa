(ns {{class}}.cards.first
  (:require [devcards.core :refer-macros [defcard]]
            [reagent.core :as r]))

(defcard greet
  (r/as-element [:h1 "Devcards is freaking awesome!"]))


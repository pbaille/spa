(ns {{class}}.utils-test
  (:require [{{class}}.utils :as sut]
            [cljs.test :as t :include-macros true]))

(t/deftest test-add
  (t/is (sut/add 1 1) 2))

(ns ^:figwheel-hooks {{class}}.test-runner
  (:require [{{class}}.core-test]
            [cljs.test :as test]
            [cljs-test-display.core :as display]))

(enable-console-print!)

(defn test-run []
  (test/run-tests
   (display/init! "app-tests")
   {{class}}.core-test))

(defn ^:after-load render-on-relaod []
  (test-run))

(test-run)

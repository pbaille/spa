(ns clj.new.spa
  (:require [clj.new.templates :as ts]
            [me.raynes.fs :as fs]
            [clojure.java.io :as io]))

;; files -------------------------------------------

(defn path [x] (.getPath x))

(defn all-files [x]
  (if (fs/directory? x)
    (mapcat all-files (fs/list-dir x))
    [x]))

;; impl --------------------------------------------

(def TEMPLATE (io/resource "clj/new/spa"))

(defn- remove-template-prefix
  "format path to be used by ts/->files"
  [p] (subs p (-> TEMPLATE io/file path count inc)))

(def ALL_TEMPLATE_FILES_PATHS
  "all unprefixed TEMPLATE's file paths"
  (->> (all-files TEMPLATE)
    (map path)
    (map remove-template-prefix)))

(def render (ts/renderer "spa"))

(defn path-seq
  "format ALL_TEMPLATE_FILES
   into a clj.new.templates/->files path-seq"
  [data]
  (map (fn [p] [p (render p data)])
       ALL_TEMPLATE_FILES_PATHS))

(defn derive-name [name]
  {:name (ts/project-name name)
   :class (ts/sanitize-ns name)
   :path (ts/name-to-path name)})

;; main ---------------------------------------------

(defn spa
  [name]
  (println "Generating awesome cljs project")
  (let [data (derive-name name)]
    (apply ts/->files
           data (path-seq data))))
